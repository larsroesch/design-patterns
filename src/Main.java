import businessObjects.ITrainer;
import dataLayer.DataLayerManager;
import dataLayer.IDataLayer;
import dataLayer.businessObjects.Trainer;
import dataLayer.dataAccessObjects.ITrainerDao;
import dataLayer.dataAccessObjects.sqlite.DataLayerSqlite;
import dataLayer.dataAccessObjects.sqlite.TrainerDaoSqlite;
import dataLayer.settings.SettingsManager;

import java.sql.SQLException;
import java.util.List;

public class Main {

    public static void main(String[] args) throws SQLException, ClassNotFoundException {


        // 1. Instanz von DatalayerManager holen
        DataLayerManager datalayerManager = DataLayerManager.getInstance();
        // 2. Den richtigen Datalayer bekommen
        IDataLayer dataLayer = datalayerManager.getDataLayer();
        // 3. Die richtige DAO bekommen
        ITrainerDao trainerDao = dataLayer.getTrainerDao();

        ITrainer newTrainer = trainerDao.create();
        newTrainer.setName("Christoph Supertrainer");
        newTrainer.setAlter(43);
        newTrainer.setErfahrung(8);

        trainerDao.save(newTrainer);

        trainerDao.select(7);

    }
}