package dataLayer;

import dataLayer.dataAccessObjects.sqlite.DataLayerSqlite;
import dataLayer.settings.SettingsManager;

public class DataLayerManager {

    private static DataLayerManager instance;
    private IDataLayer dataLayer;

    /**
     * private constructor
     */
    private DataLayerManager() {
    }

    /**
     * returns instance of DataLayerManager
     * Use Singleton Pattern
     *
     * @return DataLayerManager
     */
    public static DataLayerManager getInstance() {
        if (instance == null) {
            instance = new DataLayerManager();
        }
        return instance;
    }

    public IDataLayer getDataLayer() {


        if (dataLayer == null) {

            String type = SettingsManager.getInstance().getPersistenceSettings().getType();

            switch(type){

                case "SQlite":
                    dataLayer = new DataLayerSqlite();
                    break;

               // case "XML":
                 //   dataLayer = new DataLayerXml();
                   // break;

                default:
                    dataLayer = new DataLayerSqlite();
                    break;
            }
        }

        return dataLayer;
    }
}
