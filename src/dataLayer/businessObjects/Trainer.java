package dataLayer.businessObjects;

import businessObjects.ITrainer;

public class Trainer implements ITrainer {
    private int id;
    private String name;
    private int alter;
    private int erfahrung;


    public void setId(int id){
        this.id = id;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getAlter() {
        return this.alter;
    }

    @Override
    public void setAlter(int alter) {
        this.alter = alter;
    }

    @Override
    public int getErfahrung() {
        return this.erfahrung;
    }

    @Override
    public void setErfahrung(int erfahrung) {
        this.erfahrung = erfahrung;
    }

    @Override
    public String toString() {
        return "Trainer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", alter=" + alter +
                ", erfahrung=" + erfahrung +
                '}';
    }
}
