package dataLayer.dataAccessObjects;

import businessObjects.ITrainer;

import java.util.List;

public interface ITrainerDao {

    /**
     * Method used to create a ITrainer Object
     *
     * @return a new ITrainer
     */
    ITrainer create();


    /**
     * Method used to create a ITrainer Object
     */
    void delete(ITrainer trainer);


    /**
     * Method used to access the first entry in the DB
     *
     * @return the first ITrainer Object present in the DB
     */
    ITrainer first();


    /**
     * Method used to access the last entry in the DB
     *
     * @return the last ITrainer Object present in the DB
     */
    ITrainer last();

    /**
     * Method used to verify if there is any other entry present next to the
     * given object in the DB
     *
     * @param trainer
     * @return the next entry to the given ITrainer object present in the DB
     */
    ITrainer next(ITrainer trainer);


    /**
     * Method used to verify if there is any other entry present  in the DB
     *
     * @param trainer
     * @return the previous entry to the given ITrainer object present in the DB
     */
    ITrainer previous(ITrainer trainer);

    /**
     * Method used to save a ITrainer object in the DB
     *
     * @param trainer
     * @return the previous entry to the given ITrainer object present in the DB
     */
    void save(ITrainer trainer);

    /**
     * Method used to select all the entries in the DB
     *
     * @return a List of ITrainer objects
     */
    List<ITrainer> select();


    /**
     * Method used to select an entry in the DB
     *
     * @param id of the requested object
     * @return a a ITrainer object
     */
    ITrainer select(int id);

}
