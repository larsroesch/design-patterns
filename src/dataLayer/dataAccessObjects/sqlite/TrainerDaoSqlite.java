package dataLayer.dataAccessObjects.sqlite;

import businessObjects.ITrainer;
import dataLayer.businessObjects.Trainer;
import dataLayer.dataAccessObjects.ITrainerDao;
import exceptions.NoTrainerFoundException;
import exceptions.NoNextTrainerFoundException;
import exceptions.NoPreviousTrainerFoundException;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TrainerDaoSqlite implements ITrainerDao {

    private final String CLASSNAME = "org.sqlite.JDBC";
    private String datei = "Trainer.db3";
    private String url = "jdbc:sqlite:" + datei;
    private final String CONNECTION = url;

    private Connection connect() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(CONNECTION);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return conn;
    }

    @Override
    public ITrainer create() {

        return new Trainer();

    }

    @Override
    public void delete(ITrainer trainer) {

        String sql = "DELETE FROM trainer WHERE id = ?";

        try (Connection conn = this.connect();
             PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setInt(1, trainer.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    @Override
    public ITrainer first() {

        ITrainer trainer = new Trainer();

        String sql = "SELECT * FROM trainer ORDER BY id ASC LIMIT 1";
        try (Connection conn = this.connect();
             Statement statement = conn.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)) {

            while (resultSet.next()) {
                System.out.println(resultSet.getInt("id") + "\t" +
                        resultSet.getString("name") + "\t");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return trainer;
    }

    @Override
    public ITrainer last() {
        ITrainer trainer = new Trainer();

        String sql = "SELECT * FROM trainer ORDER BY id DESC LIMIT 1";
        try (Connection conn = this.connect();
             Statement statement = conn.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)) {

            while (resultSet.next()) {
                System.out.println(resultSet.getInt("id") + "\t" +
                        resultSet.getString("name") + "\t");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return trainer;
    }

    @Override
    public ITrainer next(ITrainer trainer) {

        ITrainer nextTrainer = null;

        int id = trainer.getId();

        String sql = "SELECT * FROM trainer WHERE id > ? ORDER BY id ASC LIMIT 1";

        try (Connection conn = this.connect();
             PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, id);

            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                nextTrainer = new Trainer();
                ((Trainer) nextTrainer).setId(resultSet.getInt("id"));
                ((Trainer) nextTrainer).setName(resultSet.getString("name"));
                ((Trainer) nextTrainer).setAlter(resultSet.getInt("age"));
                ((Trainer) nextTrainer).setErfahrung(resultSet.getInt("experience"));

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        try {
            if (nextTrainer != null) {
                return nextTrainer;
            } else {
                throw new NoNextTrainerFoundException("We couldn't find no trainer after " + trainer.getName());
            }
        } catch (NoNextTrainerFoundException e) {
            JOptionPane.showMessageDialog(JOptionPane.getRootFrame(), e.getMessage());
            return trainer;
        }
    }

    @Override
    public ITrainer previous(ITrainer trainer) {

        ITrainer previoustTrainer = null;

        int id = trainer.getId();

        String sql = "SELECT * FROM trainer WHERE id < ? ORDER BY id DESC LIMIT 1";

        try (Connection conn = this.connect();
             PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, id);

            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                previoustTrainer = new Trainer();
                ((Trainer) previoustTrainer).setId(resultSet.getInt("id"));
                ((Trainer) previoustTrainer).setName(resultSet.getString("name"));
                ((Trainer) previoustTrainer).setAlter(resultSet.getInt("age"));
                ((Trainer) previoustTrainer).setErfahrung(resultSet.getInt("experience"));

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        try {
            if (previoustTrainer != null) {
                return previoustTrainer;
            } else {
                throw new NoPreviousTrainerFoundException("We couldn't find no trainer before " + trainer.getName());
            }
        } catch (NoPreviousTrainerFoundException e) {
            System.out.println(e.getMessage());
            return trainer;
        }
    }

    @Override
    public void save(ITrainer trainer) {

        if (select(trainer.getId()) == null) {

            String sql = "INSERT INTO trainer(name, age, experience) VALUES(?,?,?)";

            try (Connection conn = this.connect();
                 PreparedStatement statement = conn.prepareStatement(sql)) {
                statement.setString(1, trainer.getName());
                statement.setInt(2, trainer.getAlter());
                statement.setInt(3, trainer.getErfahrung());
                statement.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        } else {
            String sql = "UPDATE trainer name = ?, age = ?, experience = ? WHERE id = ?";

            try (Connection conn = this.connect();
                 PreparedStatement statement = conn.prepareStatement(sql)) {
                statement.setString(1, trainer.getName());
                statement.setInt(2, trainer.getAlter());
                statement.setInt(3, trainer.getErfahrung());
                statement.setInt(4, trainer.getId());
                statement.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }

        }
    }

    @Override
    public List<ITrainer> select() {
        ITrainer trainer = null;
        List<ITrainer> list = new ArrayList<>();

        String sql = "SELECT * FROM trainer";

        try (Connection conn = this.connect();
             PreparedStatement stmt = conn.prepareStatement(sql)) {

            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                trainer = new Trainer();
                ((Trainer) trainer).setId(resultSet.getInt("id"));
                ((Trainer) trainer).setName(resultSet.getString("name"));
                ((Trainer) trainer).setAlter(resultSet.getInt("age"));
                ((Trainer) trainer).setErfahrung(resultSet.getInt("experience"));

                list.add(trainer);

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        try {
            if (!list.isEmpty()) {
                return list;
            } else {
                throw new NoTrainerFoundException("We couldn't find any trainer with id.");
            }
        } catch (NoTrainerFoundException e) {
            JOptionPane.showMessageDialog(JOptionPane.getRootFrame(), e.getMessage());
            return null;
        }
    }

    @Override
    public ITrainer select(int id) {

        ITrainer trainer = new Trainer();

        String sql = "SELECT * FROM trainer WHERE id = ?";

        try (Connection conn = this.connect();
             PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, id);
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {

                ((Trainer) trainer).setId(resultSet.getInt("id"));
                ((Trainer) trainer).setName(resultSet.getString("name"));
                ((Trainer) trainer).setAlter(resultSet.getInt("age"));
                ((Trainer) trainer).setErfahrung(resultSet.getInt("experience"));

                System.out.println(trainer.toString());
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        try {
            if (trainer.getName() != null) {
                return trainer;
            } else {
                throw new NoTrainerFoundException("We couldn't find any trainer with id " + id);
            }
        } catch (NoTrainerFoundException e) {
            JOptionPane.showMessageDialog(JOptionPane.getRootFrame(), e.getMessage());
            return null;
        }
    }
}
