package dataLayer.dataAccessObjects.xml;

import dataLayer.IDataLayer;
import dataLayer.dataAccessObjects.ITrainerDao;

/**
 * DataLayerXml
 *
 * @author Lars Rösch
 * @version 26.09.2019
 */
public class DataLayerXml implements IDataLayer {

    private ITrainerDao trainerDaoXml = new TrainerDaoXml();

    @Override
    public ITrainerDao getTrainerDao() {
        return trainerDaoXml;
    }
}
