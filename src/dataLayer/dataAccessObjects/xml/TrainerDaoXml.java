package dataLayer.dataAccessObjects.xml;

import businessObjects.ITrainer;
import dataLayer.businessObjects.Trainer;
import dataLayer.dataAccessObjects.ITrainerDao;

import exceptions.NoNextTrainerFoundException;
import exceptions.NoPreviousTrainerFoundException;
import jdk.internal.org.xml.sax.SAXException;
import org.w3c.dom.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;

import java.util.ArrayList;
import java.util.List;

/**
 * TrainerDaoXml
 *
 * @author Lars Rösch
 * @version 26.09.2019
 */
public class TrainerDaoXml implements ITrainerDao {

    private Document trainerDB;
    NodeList nList;

    TrainerDaoXml() {
        try {
            //Create the File
            File xmlFile = new File("src\\Trainer");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            this.trainerDB = dBuilder.parse(xmlFile);
            // Database auslesen
            this.trainerDB.getDocumentElement().normalize();
            System.out.println("Root element :" + trainerDB.getDocumentElement().getNodeName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public ITrainer create() {
        return new Trainer();
    }

    @Override
    public void delete(ITrainer trainer) {
        Element e = (Element) trainerDB.getElementsByTagName("trainer").item(trainer.getId() - 1);
        e.getParentNode().removeChild(e);
        saveDB();
    }

    @Override
    public ITrainer first() {
        return select().get(0);
    }

    @Override
    public ITrainer last() {
        Trainer trainer;
        if (select().get(select().size() - 1) == null)
            trainer = null;
        else
            trainer = (Trainer) select().get(select().size() - 1);
        return trainer;
    }

    @Override
    public ITrainer next(ITrainer trainer) {
        Trainer trainerThis;
        try {
            if (trainer.getId() < select().size())
                trainerThis = (Trainer) select().get(trainer.getId());
            else
                throw new NoNextTrainerFoundException("We couldn't find any trainer after " + trainer.getName());
        } catch (NoNextTrainerFoundException e) {
            System.out.println(e.getMessage());
            trainerThis = null;
        }
        return trainerThis;
    }

    @Override
    public ITrainer previous(ITrainer trainer) {
        Trainer trainerThis;
        try {
            if ((trainer.getId() - 2) >= 0)
                trainerThis = (Trainer) select().get(trainer.getId() - 2);
            else
                throw new NoPreviousTrainerFoundException("We couldn't find any trainer before " + trainer.getName());
        } catch (NoPreviousTrainerFoundException e) {
            System.out.println(e.getMessage());
            trainerThis = null;
        }
        return trainerThis;
    }

    @Override
    public void save(ITrainer trainer) {
        if (trainer.getId() == 0) {
            // Ein neuer Trainer wird angelegt
            ((Trainer) (trainer)).setId(trainerDB.getDocumentElement().getElementsByTagName("trainer").getLength() + 1);
            saveNewTrainer(trainer);
        } else {
            int id = trainer.getId() - 1;
            nList = trainerDB.getElementsByTagName("trainer");
            Node nNode = nList.item(id);
            Element eElement = (Element) nNode;
            if (Integer.parseInt(eElement.getElementsByTagName("id").item(0).getTextContent()) == trainer.getId()) {
                // Vorhander Trainer wird überschrieben
                nList = saveExistingTrainer(trainer);
            }
        }
        saveDB();

    }

    @Override
    public List<ITrainer> select() {
        List<ITrainer> trainerList = new ArrayList<>();
        Trainer trainer;
        nList = trainerDB.getElementsByTagName("trainer");
        Node nNode;
        Element eElement;
        for (int i = 0; i < nList.getLength(); i++) {
            trainer = new Trainer();
            nNode = nList.item(i);
            eElement = (Element) nNode;
            // ID
            trainer.setId(Integer.parseInt(eElement.getElementsByTagName("id").item(0).getTextContent()));
            // Name
            trainer.setName((eElement.getElementsByTagName("name").item(0).getTextContent()));
            // Alter
            trainer.setAlter(Integer.parseInt(eElement.getElementsByTagName("alter").item(0).getTextContent()));
            // Erfahrung
            trainer.setErfahrung(Integer.parseInt(eElement.getElementsByTagName("erfahrung").item(0).getTextContent()));
            trainerList.add(trainer);
            trainer = null;
        }
        return trainerList;
    }

    @Override
    public ITrainer select(int id) {
        id = id - 1;
        Trainer trainer = new Trainer();
        nList = trainerDB.getElementsByTagName("trainer");
        Node nNode = nList.item(id);
        Element eElement = (Element) nNode;
        if (nNode != null) {
            // ID
            trainer.setId(Integer.parseInt(eElement.getElementsByTagName("id").item(0).getTextContent()));
            // Name
            trainer.setName((eElement.getElementsByTagName("name").item(0).getTextContent()));
            // Alter
            trainer.setAlter(Integer.parseInt(eElement.getElementsByTagName("alter").item(0).getTextContent()));
            // Erfahrung
            trainer.setErfahrung(Integer.parseInt(eElement.getElementsByTagName("erfahrung").item(0).getTextContent()));
        } else {
            trainer = null;
        }
        return trainer;
    }

    private void saveNewTrainer(ITrainer trainer) {
        Node item = null;
        Element root = trainerDB.createElement("trainer");
        // ID
        item = trainerDB.createElement("id");
        item.appendChild(trainerDB.createTextNode(trainer.getId() + ""));
        root.appendChild(item);
        // Name
        item = trainerDB.createElement("name");
        item.appendChild(trainerDB.createTextNode(trainer.getName()));
        root.appendChild(item);
        // Alter
        item = trainerDB.createElement("alter");
        item.appendChild(trainerDB.createTextNode("" + trainer.getAlter()));
        root.appendChild(item);
        // Erfahrung
        item = trainerDB.createElement("erfahrung");
        item.appendChild(trainerDB.createTextNode("" + trainer.getErfahrung()));
        root.appendChild(item);
        trainerDB.getDocumentElement().appendChild(root);
    }

    private NodeList saveExistingTrainer(ITrainer trainer) {
        NodeList nList = trainerDB.getElementsByTagName("trainer");
        Node nNode = nList.item(trainer.getId() - 1);
        Element eElement = (Element) nNode;
        // ID
        eElement.getElementsByTagName("id").item(0).setTextContent("" + trainer.getId());
        // Name
        eElement.getElementsByTagName("name").item(0).setTextContent(trainer.getName());
        // Alter
        eElement.getElementsByTagName("alter").item(0).setTextContent("" + trainer.getAlter());
        // Erfahrung
        eElement.getElementsByTagName("erfahrung").item(0).setTextContent("" + trainer.getErfahrung());
        nNode = eElement;
        nList.item(trainer.getId() - 1).setNodeValue(nNode.getNodeValue());
        return null;
    }

    private void saveDB() {
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            Result output = new StreamResult(new File("src\\Trainer"));
            Source input = new DOMSource(trainerDB);
            transformer.transform(input, output);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}


