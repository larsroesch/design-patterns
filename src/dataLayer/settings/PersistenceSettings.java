package dataLayer.settings;

/**
 * PersistenceSettings
 *
 * @author Lars Rösch
 * @version 26.09.2019
 */
public class PersistenceSettings {
    // Constructor Default
    PersistenceSettings(){
        this.type = "SQlite";
    }

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
