package dataLayer.settings;

/**
 * SettingsManager
 *
 * @author Lars Rösch
 * @version 26.09.2019
 */
public class SettingsManager {
    private static SettingsManager instance;
    private PersistenceSettings persistenceSettings;

    private SettingsManager(){
        this.persistenceSettings = new PersistenceSettings();
    }


    public static SettingsManager getInstance() {
        if (instance == null){
            instance = new SettingsManager();
        }
        return instance;
    }

    public PersistenceSettings getPersistenceSettings() {
        return persistenceSettings;
    }

    private String readPersistenceSettings(){
        return getPersistenceSettings().getType();
    }

}
