package exceptions;

public class NoNextTrainerFoundException extends Exception {

    private String exceptionMessage;

    public NoNextTrainerFoundException(String exceptionMessage){
        super();
        this.exceptionMessage = exceptionMessage;
    }

    @Override
    public String getMessage(){
        return "Exception: " + exceptionMessage;
    }
}
