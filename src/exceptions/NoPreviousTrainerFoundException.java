package exceptions;

public class NoPreviousTrainerFoundException extends Exception {

    private String exceptionMessage;

    public NoPreviousTrainerFoundException(String exceptionMessage) {
        super();
        this.exceptionMessage = exceptionMessage;
    }

    @Override
    public String getMessage() {
        return "Exception: " + this.exceptionMessage;
    }
}
