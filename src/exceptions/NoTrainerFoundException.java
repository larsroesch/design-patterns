package exceptions;

public class NoTrainerFoundException extends Exception {

    private String exceptionMessage;

    public NoTrainerFoundException(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    @Override
    public String getMessage() {
        return "Exception: " + exceptionMessage;
    }
}
